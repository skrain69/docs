import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Basics & Concepts',
    Svg: require('../../static/img/basics.svg').default,
    description: (
      <>
        Learn the basics of Kotlin including concepts that will help you become more productive and create higher quality applications.
      </>
    ),
  },
  {
    title: 'Object-Oriented Programming',
    Svg: require('../../static/img/oop.svg').default,
    description: (
      <>
        Learn how to solve complex problems and write code using an object-oriented way of thinking - safe, concise, compact, pragmatic, and saves you time. 
      </>
    ),
  },
  {
    title: 'REST API with Spring Boot',
    Svg: require('../../static/img/springboot.svg').default,
    description: (
      <>
        Learn how to organize your thoughts with heaps of available information to properly build a REST API using Spring Boot.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
