---
sidebar_position: 1
---

# Cheat Sheets
<!-- 😍 HTML Cheatsheet
https://learn-the-web.algonquindesign.ca/topics/html-semantics-cheat-sheet/
https://www.codecademy.com/learn/learn-html/modules/learn-html-elements/cheatsheet


✂️ CSS Cheatsheet
https://www.codecademy.com/learn/learn-css/modules/syntax-and-selectors/cheatsheet

📋 Intermediate CSS Cheatsheet
https://www.codecademy.com/learn/learn-intermediate-css/modules/layout-with-flexbox/cheatsheet

👌 Tailwind SS Cheatsheet
https://nerdcave.com/tailwind-cheat-sheet -->

Here are some of the cheatsheets you'll need 😋

### 😍 Kotlin Cheatsheet
https://devhints.io/kotlin

### 👌 SQL Cheatsheet
https://learnsql.com/blog/sql-basics-cheat-sheet/

### 😋 Git Cheatsheet
https://gitcheatsheet.org/?gclid=Cj0KCQjwtMCKBhDAARIsAG-2Eu-eXJi-wX0MYyu2SaSpYwW2HcAyAQ462hANwSxYV8ApiJmOxI-oLFgaAr6UEALw_wcB

### 💁 Command Line Cheatsheet
https://www.git-tower.com/blog/command-line-cheat-sheet/

### 😜 Git Workflow Cheatsheet
https://www.codecademy.com/learn/learn-git/modules/learn-git-git-workflow-u/cheatsheet

