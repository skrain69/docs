---
sidebar_position: 2
---

# Keep It Simple, Stupid 💋

KISS or popularly known as 'Keep It Simple, Stupid' or 'Keep It Super Simple' according to Kwerenachi Utosu is a very important software design principle that aims to keep your code simple, clear, and easy to understand. It is important because it puts in your subconscious that every process you're creating should be as simple as possible and also equally as efficient. 

:::tip 
Simplicity should be a key goal in design and unnecessary complexity should be avoided.
:::

## Violation of KISS 

According to most developers, messy codes and unnecessary lines may lead to delays in a project. The simpler the code is, the better, considering most systems work best if they are kept simple rather than complex. 

Below are two code snippets with both methods doing the same thing. Now you have to decide which one to use:

### Code snippet I
```java
public String weekday1(int day) {
    switch (day) {
        case 1:
            return "Monday";
        case 2:
            return "Tuesday";
        case 3:
            return "Wednesday";
        case 4:
            return "Thursday";
        case 5:
            return "Friday";
        case 6:
            return "Saturday";
        case 7:
            return "Sunday";
        default:
            throw new InvalidOperationException("day must be in range 1 to 7");
    }
}
```
### Code snippet II
```java
public String weekday2(int day) {
    if ((day < 1) || (day > 7)) throw new InvalidOperationException("day must be in range 1 to 7");

    string[] days = {
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    };

    return days[day - 1];
}
```
## How to achieve KISS 

- Practice writing simple code
- Choose the best solution to your problem
- Divide your code into multiple methods
- If you have a lot of conditions in your method, break these out into smaller methods 

## Benefits of KISS 

- Facilitates continuity and saves development time
- Allows developers to easily understand the process
- Allows developers to easily modify and maintain the existing code base
- Greater efficiency in automated testing

:::tip
When writing any code or module, make it a habit to use software design principles like KISS to save development time and help you create robust software modules efficiently.
:::