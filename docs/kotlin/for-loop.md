---
sidebar_position: 17
---

# For Loop
They are used to repeat a specific block of code until certain condition is met. It iterates through anything that provides an iterator that contains a countable number of values. 

:::tip
It is equivalent to the **foreach** loop in languages like C#.
:::

```kotlin
for (item in collection) {
  // body of loop
}
```

## Through a Range
The loop iterates through the range and prints individual item. To iterate over a range of numbers, we will use a **range expression**.

```kotlin
fun main(args: Array<String>) {
  // prints 1-5
  for (item in 1..5) {
    println(item)
  }
}

// 1
// 2
// 3
// 4
// 5
```

:::caution
The range `..` can’t be used in the reverse order.
:::

Next, the loop will iterate through another range, but this time it will **step down**, by using the `downto` keyword, instead of stepping up as in the above example.

```kotlin
fun main(args: Array<String>) {
  // iterates numbers in reverse order with 2 steps and prints them
  for (item in 5 downTo 1 step 2) {
    println(item)
  }
}

// 5
// 3
// 1
```
To exclude the last element from the range we use the keyword `until`.

```kotlin
// prints 1-3
for (i in 1 until 4) {
  print(i)
}

// 123
```

## Through a Array
**Array** is another data type which provides iterator, so we can use for loop to iterate through it in a similar way as we did it for the ranges.

```kotlin
fun main(args: Array<String>) {
  // creates an array
  var fruits = arrayOf("Orange", "Apple", "Mango", "Banana")
   
  // iterates the array and prints every element
  for (item in fruits) {
    println(item)
  }
}

// Orange
// Apple
// Mango
// Banana
```
This time we will iterate through the array using its **index**.

```kotlin
fun main(args: Array<String>) {
  // creates an array
  var fruits = arrayOf("Orange", "Apple", "Mango", "Banana")
   
  // iterates the array and prints every element using its index
  for (index in fruits.indices) {
    println(fruits[index])
  }
}

// Orange
// Apple
// Mango
// Banana
```

## Other Examples

```kotlin
fun main(args: Array<String>) {
  // iterates numbers in reverse order with 2 steps and prints them
  for (item in 6 downTo 1 step 2) {
    println(item)
  }
}

// 6 
// 4 
// 2
```

## Additional Learning Materials
🔗 [Tutorials Point](https://www.tutorialspoint.com/kotlin/kotlin_for_loop.htm)