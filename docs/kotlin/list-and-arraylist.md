---
sidebar_position: 9
---

# List and ArrayList
ArrayList class is used to create a **dynamic array** in Kotlin. Dynamic array states that we can increase or decrease the size of an array as per requisites. It also provide read and write functionalities.

## Add specific element into the ArrayList 
Using `add(index:Int, element: E): Boolean`, the **first** parameter is the index to which we want to add the element. It is optional, and by default the value of it is `1 + last index of array`. The **second** parameter contains the element to be added which is mandatory.

```kotlin
fun main(args: Array<String>) {
  // creates empty arrayList using constructor
  var arrayList = ArrayList<String>()

  //adds String elements in the list
  arrayList.add("Grey")
  arrayList.add("Owl")

  println("Array list ---->")
  // Array list ---->

  // iterates the list and prints every element
  for(i in arrayList)
    println(i)
  
  // adds "For" at index 1
  arrayList.add( 1 , "For")

  println("Arraylist after insertion ---->")
  // Arraylist after insertion ---->

  // iterates the updated list and prints every element
  for(i in arrayList)
    println(i)
}

// Array list ---->
// Grey
// Owl
// Arraylist after insertion ---->
// Grey
// For
// Owl
```
## Add elements into the current list at the specific index 
Using `addAll(index: Int, elements: Collection): Boolean`, the **first** parameter is the index to which we want to add the element. It is optional, and by default the value of it is `1 + last index of array`. The **second** parameter contains the element to be added which is mandatory.

```kotlin
fun main(args: Array<String>) {
  // creates empty arrayList using constructor
  var arrayList=ArrayList<String>()

  //adds String elements in the list
  arrayList.add("Grey")
  arrayList.add("For")
  arrayList.add("Owl")

  // creates new arrayList1
  var arrayList1 = ArrayList<String>()

  // Elements in arrayList1 -->
  println("Elements in arrayList1 -->")

  //adds all elements from arrayList to arraylist1
  arrayList1.addAll(arrayList)

  // iterates the list and prints every element
  for(i in arrayList1)
    println(i)
}

// Elements in arrayList1 -->
// Grey
// For
// Owl
```
## Return element at specific index 
Using `get(index: Int): E`

```kotlin
fun main(args: Array<String>) {
  // creates empty arraylist using constructor
  var arrayList=ArrayList<Int>()

  // adds elements
  arrayList.add(10)
  arrayList.add(20)
  arrayList.add(30)
  arrayList.add(40)
  arrayList.add(50)
  
  // iterates through and prints the elements
  for(i in arrayList)
    print("$i ")

  println()

  println("Accessing the index 2 of arraylist: "+arrayList.get(3))
  // Accessing the index 2 of arraylist: 40
}

// 10 20 30 40 50
// Accessing the index 2 of arraylist: 40
```

## Replace element from the specific position 
Using `set(index: Int, element: E):E`
```kotlin
fun main(args: Array<String>) {
  // creates empty arraylist using constructor
  var arrayList=ArrayList<String>()

  // adds elements
  arrayList.add("Grey")
  arrayList.add("for")
  arrayList.add("Owl:")
  arrayList.add("Portal")

  // iterates through and prints the elements
  for(i in arrayList)
    print("$i ")

  println()

  // sets the element at index 3 with new string
  arrayList.set(3,"A Computer Science portal for students")

  // iterates through and prints the elements
  for(i in arrayList)
    print("$i ")
}
 
// Grey for Owl: Portal 
// Grey for Owl: A Computer Science portal for students 
```

## Return index of first occurrence of specific element 
Using `indexOf(element: E): Int`
```kotlin
fun main(args: Array<String>) {
  // creates empty arraylist using constructor
  var arrayList=ArrayList<String>()

  // adds elements
  arrayList.add("Grey")
  arrayList.add("for")
  arrayList.add("Owl")
  
  // iterates through and prints the elements
  for(i in arrayList)
    print("$i ")
  
  // next line
  println()

  println("The index of the element is: "+arrayList.indexOf("Grey"))
  // The index of the element is: 0
}

// Grey for Owl 
// The index of the element is: 0
```

## Remove specific element's first occurrence 
Using `remove(element: E): Boolean`.
For removing element at index `i` we use `removeAt(index)`.

```kotlin
fun main(args: Array<String>) {
  // creates empty arraylist using constructor
  var arrayList=ArrayList<String>()

  // adds elements
  arrayList.add("Grey")
  arrayList.add("for")
  arrayList.add("Owl")
  
  // removes "for" from the arraylist
  arrayList.remove("for")

  // iterates through and prints the elements
  for(i in arrayList)
    print("$i ")
}

```

## Remove all elements  
Using `clear()`
```kotlin
fun main(args: Array<String>) {
  // creates empty arraylist using constructor
  var arrayList = ArrayList<Int>()

  // adds elements
  arrayList.add(10)
  arrayList.add(20)
  arrayList.add(30)
  arrayList.add(40)
  arrayList.add(50)
  
  // iterates through and prints the elements
  for(i in arrayList)
    print("$i ")

  // removes all elements from the list
  arrayList.clear()

  // next line
  println()

  println("The size of arraylist after clearing all elements: "+arrayList.size)
  // The size of arraylist after clearing all the elements: 0
}

// 10 20 30 40 50 
// The size of arraylist after clearing all the elements: 0
```

## Additional Learning Materials
🔗 [Geeks for Geeks](https://www.geeksforgeeks.org/kotlin-list-arraylist/)