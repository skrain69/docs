---
sidebar_position: 2
---

# Variables and Strings

A **variable** is like a container for data. It is an abstract storage location paired with an associated symbolic name, which contains some known or unknown quantity of information referred to as a *value*. On the other hand, **strings** can contain any sequence of characters. It is a *data type* used for data values that are made up of ordered sequences of characters, such as "hello world". Moreover, `println()` method is often used to display what is inside the parenthesis.

## Variables
They are declared by using the keyword `var` (for mutable variables) or `val` (for read-only variables). This is similar to `let` and `const` in JavaScript ES5. Use `val` for a variable whose value never changes, and `var` for a variable whose value can change.

### Variable Declaration
Like Java, Kotlin is a statically-typed language—the variable type is written after the variable name, separated by a colon `:`. 
```kotlin
// read-only
val x:Int = 448  

// mutable
var y:Int = 9000  
```

Additionally, variable type can often be inferred from the assigned value, so may be omitted:
```kotlin
// `Int` type is inferred
val x = 448  
```

Without the explicit type declaration, the variable type is determined by the compiler. 

## Strings
They are represented by the type `String` . Generally, a string value is a sequence of characters in double quotes `"`.

```kotlin
val str = "abcd 123" 
```

Elements of a string are characters that you can access via the indexing operation `s[i]`, where `i` is the index of that character, starting with 0. We will discuss that further in the upcoming lessons.

:::caution caution
Strings are immutable. 
:::

Once you initialize a string, you can't change its value or assign a new value to it. All operations that transform strings return their results in a new `String` object, leaving the original string unchanged. 
```kotlin
// creates and prints a new String object
val str = "abcd"
println(str.uppercase()) 
// ABCD 

// the original string remains the same
println(str) 
// abcd
```
### String Concatentation
To concatenate strings, use the `+` operator. 
This also works for concatenating strings with values of other types, as long as the first element in the expression is a string. For example:

```kotlin
val s = "abc" + 1
// s becomes abc1

println(s + "def") 
// abc1def
```

### String literals 
There are two types of string literals:

👌 Escaped strings that may contain escaped characters<br/>
👌 Raw strings that can contain newlines and arbitrary text

Escaping is done in the conventional way, with a backslash `\`. For example:
```kotlin
val s = "Hello, world!\n"
```
A raw string is delimited by a triple quote `"""`, contains no escaping and can contain newlines and any other characters. For example: 
```kotlin
val text = """
  for (c in "foo")
    print(c)
"""
```
### String Templates
Template expressions are pieces of code that are evaluated and whose results are concatenated into the string. This is also often preferred to string concatenation. A **template expression** starts with a dollar sign `$` and consists of either a name:

```kotlin
val i = 10

println("i = $i") 
// i = 10
```
or an expression in curly braces:
```kotlin
val s = "abc"

println("$s.length is ${s.length}") 
// abc.length is 3
```

## Additional Learning Materials
🔗 [Learn the Kotlin Programming Language | Android Developers](https://developer.android.com/kotlin/learn) <br/>
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/basic-types.html)