---
sidebar_position: 7
---

# Exception Handling 

Exceptions are unwanted issues that can occur at runtime of the program and terminate your program abruptly. They can break your program. Using Exception Handling, you can handle failed conditions gracefully and continue with your program flow.

**There are two types of exceptions:**<br/>
👌 Checked exceptions that are declared as part of method signature and are checked at compile time. An example would be `IOException`<br/>
👌 Unchecked exceptions do not need to be added as part of method signature and they are checked at runtime. An example would be `NullPointerException`.

:::note
In Kotlin, all exceptions are unchecked.
:::

## Try-catch-finally block
To handle exceptions we use the try-catch-finally block. The piece of code that has a possibility to give an exception is set inside the `try` block. The exception is caught inside the `catch` block.

```kotlin
fun main(args: Array<String>) {
  try {
    var a = 0
    var x = 7 / a

    val v = "theBootcamp PH"
    v.toInt()

  } catch (e: ArithmeticException) {
    println("Arthimetic Exception")
  } catch (e: Exception) {
    println("Exception occured. To print stacktrace use e")
  } finally {
    println("Finally. It's over")
  }
}

// Arthimetic Exception
// Finally. It's over
```

The `finally` block is optional. It’s generally used to release any resources that were used in the try-catch.
Typically a `try` can have multiple catches. Only the first matching catch would be run. 

:::tip
It's recommended to stack the catches in the order: Specific Exception to General Exception.
:::

Once an exception in the try block is found, it’ll not execute anything else in that try block.

## How to throw an exception in Kotlin
We can also throw an exception using `throw` keyword. In this example, The statement before the exception got executed, but the statement after the exception didn’t execute because the control transferred to the `catch` block.

```kotlin
fun main(args: Array<String>) {
  try {
    println("Before exception")
    throw Exception("Something went wrong here")
    println("After exception")
  }
  catch(e: Exception){
    println(e)

  }
  finally{
    println("You can't ignore me")
  }
}

// Before exception
// java.lang.Exception: Something went wrong here
// You can't ignore me
```

The following example should throw `ArithmeticException`. Since this code is in try block, the corresponding `catch` block will execute. In this case the `ArithmeticException` occurred so the `catch` block of `ArithmeticException` was executed and “Arithmetic Exception” is printed in the output.

```kotlin
fun main(args: Array<String>) {
  try {
    var num = 10/0
    println("BeginnersBook.com")
    println(num)

  } catch (e: ArithmeticException) {
    println("Arithmetic Exception")
  } catch (e: Exception) {
    println(e)
  } finally {
    println("It will print in any case.")
  }
}

// Arithmetic Exception
// It will print in any case.
```
When an exception occurs, it ignores everything after that point and the control instantly jumps to the catch block. The `finally` block is always executed whether exception occurs or not.

## Additional Learning Materials
🔗 [Beginners Book](https://beginnersbook.com/2019/03/kotlin-exception-handling/) 