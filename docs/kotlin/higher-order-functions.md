---
sidebar_position: 23
---

# Higher Order Functions
When we define a function that accepts a function as an argument or returns a function from it, it's called a **higher order function**.

## Defining a Higher Order Function

```kotlin
// accepts a function as an argument
fun rollDice(
  range: IntRange,
  time: Int,
  callback: (result: Int) -> Unit
) {
  for (i in 0 until time) {
    val result = range.random()

    callback(result)
  }
}
```

## Calling a Higher Order Function
```kotlin
fun main() {
  rollDice(1..6, 3, { result ->
    println(result)
  })
}

// 1
// 6
// 1
 ```

The function `rollDice()`  is the higher order function that is accepting another function as an **argument**. And the function that we have defined as the argument is the last parameter. When we have this situation, we can put the **lambda expression** outside the parenthesis while calling the function.

```kotlin
fun main() {
  rollDice(1..6, 3) { result ->
    println(result)
  }
}

// 3
// 4
// 2
```
## it Keyword
When we have only a **single parameter** in our lambda expression, we can omit the parameter name and instead of writing a name we can use `it` keyword to get the parameter.

```kotlin
fun main() {
  rollDice(1..6, 3) { 
    println(it)
  }
}

// 2
// 5
// 2
```

When your function is having only a **single parameter** that is a function, you can omit the parenthesis.

```kotlin
fun rollDice(callback: (result: Int) -> Unit) {
  callback((1..6).random())
}
 
fun main() {
  rollDice {
    println(it)
  }
}
```
## Null as the Default Value
We can also assign `null` as the **default value**.

```kotlin
fun rollDice(callback: ((result: Int) -> Unit)? = null) {
  callback?.invoke((1..6).random())
}
 
fun main() {
  rollDice {
    println(it)
  }
}
```
When you have null as the default value, you can use `invoke()`  to call the function.

## Importance of Higher Order Functions
Imagine a scenario when you are doing some heavy operation (that requires long time to execute) in a different thread inside a function. After finishing the task, you need a **callback**. You can achieve this easily by using **higher order functions**.

```kotlin
fun rollDice(callback: ((result: Int) -> Unit)? = null) {
  println("Roll Dice Started")
  // Roll Dice Started

  thread {
    // mimicking a heavy operation by just putting a Thread.sleep

    Thread.sleep(5000)

    // function was already finished but after 5 seconds
    // we will get a callback at the trailing lambda

    callback?.invoke((1..6).random())
  }
  println("Roll Dice Ended")
  // Roll Dice Ended
}
 
fun main() {
  rollDice {
    println(it)
  }
}
```

## Additional Learning Materials
🔗 [Simplified Coding](https://www.simplifiedcoding.net/higher-order-functions-kotlin/)