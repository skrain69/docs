---
sidebar_position: 1
---

# Introduction to Kotlin

Welcome!

Kotlin is inspired by existing languages such as Java, C#, JavaScript, Scala and Groovy. It is a relatively new programming language developed by **JetBrains** for modern multiplatform applications. It is expressive, concise, open-source, and type-safe.

It's initially designed for the JVM (Java Virtual Machine) and Android that combines object-oriented and functional programming features. It is focused on interoperability, safety, clarity, and tooling support.

The language also supports higher-order functions, anonymous functions, lambdas, inline functions, closures, tail recursion, and generics. In other words, Kotlin has all of the features of a **functional** language. 

The advantages of Kotlin are compelling. The typical time quoted for a Java developer to learn Kotlin is a few hours—a small price to pay to eliminate null reference errors, enable extension functions, support functional programming, and add coroutines.

It's supported by all major Java IDEs including **IntelliJ IDEA**, **Android Studio**, and **Eclipse**. 