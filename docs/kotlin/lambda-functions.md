---
sidebar_position: 21
---

# Lambda Functions
Lambda expressions and anonymous functions are **function literals**. Function literals are functions that are not declared but are passed immediately as an expression. 

```kotlin
val sum: (Int, Int) -> Int = { x: Int, y: Int -> x + y }
```

The function `max` is a higher-order function, as it takes a function value as its second argument. This second argument is an **expression** that is itself a function, called a **function literal**, which is equivalent to the following named function.

```kotlin
max(strings, { a, b -> a.length < b.length })
```

```kotlin
fun compare(a: String, b: String): Boolean = a.length < b.length
```

:::note
A **lambda expression** is always surrounded by curly braces.
:::

If you leave all the optional annotations out, it looks like this.

```kotlin
val sum = { x: Int, y: Int -> x + y }
```

## Passing trailing lambdas
If the **last parameter** of a function is a function, then a lambda expression passed as the corresponding argument can be placed outside the parentheses.

```kotlin
val product = items.fold(1) { acc, e -> acc * e }
```
This is also known as **trailing lambda**. If the lambda is the only argument in that call, the parentheses can be **omitted** entirely.

```kotlin
run { println("...") }
```

## Implicit name of a single parameter
It's very common for a lambda expression to have only **one** parameter. If the compiler can parse the signature without any parameters, the parameter does not need to be declared and `->` can be omitted. The parameter will be implicitly declared under the name `it`.

```kotlin
ints.filter { it > 0 } // this literal is of type '(it: Int) -> Boolean'
```

## Returning a value from a lambda expression
You can explicitly return a value from the lambda using the qualified return syntax. Otherwise, the value of the last expression is implicitly returned.

```kotlin
ints.filter {
  val shouldFilter = it > 0
  shouldFilter
}

// same as

ints.filter {
  val shouldFilter = it > 0
  return@filter shouldFilter
}
```

This convention, along with passing a lambda expression outside of parentheses, allows for ***LINQ-style*** code.

```kotlin
strings.filter { it.length == 5 }.sortedBy { it }.map { it.uppercase() }
```

## Underscore for unused variables
If the lambda parameter is **unused**, you can place an underscore instead of its name.
```kotlin
map.forEach { _, value -> println("$value!") }
```

## Destructuring in lambdas

```kotlin
{ a -> ... } // one parameter
{ a, b -> ... } // two parameters
{ (a, b) -> ... } // a destructured pair
{ (a, b), c -> ... } // a destructured pair and another parameter
```

```kotlin
map.mapValues { entry -> "${entry.value}!" }
map.mapValues { (key, value) -> "$value!" }
```

If a component of the destructured parameter is **unused**, you can replace it with the underscore to avoid inventing its name.
```kotlin
map.mapValues { (_, value) -> "$value!" }
```
You can specify the type for the whole destructured parameter or for a specific component separately.
```kotlin
map.mapValues { (_, value): Map.Entry<Int, String> -> "$value!" }

map.mapValues { (_, value: String) -> "$value!" }
```

## Adding two integers

```kotlin
fun main(args: Array<String>){
  val sum = {num1: Int, num2: Int -> num1 + num2}

  println("10+5: ${sum(10,5)}")
  // 10+5: 15
}
```

## Additional Learning Materials
🔗 [Beginners Book](https://beginnersbook.com/2019/03/kotlin-lambda-function/)
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/lambdas.html#lambda-expressions-and-anonymous-functions)