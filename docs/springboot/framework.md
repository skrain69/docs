---
sidebar_position: 1
---

# Spring Framework

The Spring Framework provides a comprehensive programming and configuration model for modern Java-based enterprise applications - on any kind of deployment platform.

A key element of Spring is infrastructural support at the application level: Spring focuses on the "plumbing" of enterprise applications so that teams can focus on application-level business logic, without unnecessary ties to specific deployment environments.

## Features

- **Core technologies:** dependency injection, events, resources, i18n, validation, data binding, type conversion, SpEL, AOP.

- **Testing:** mock objects, TestContext framework, Spring MVC Test, WebTestClient.

- **Data Access:** transactions, DAO support, JDBC, ORM, Marshalling XML.

- Spring MVC and Spring WebFlux web frameworks.

- **Integration:** remoting, JMS, JCA, JMX, email, tasks, scheduling, cache.

- **Languages:** Kotlin, Groovy, dynamic languages.

<iframe width="950" height="600" src="https://www.youtube.com/embed/gq4S-ovWVlM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
