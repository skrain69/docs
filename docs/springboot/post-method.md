---
sidebar_position: 13
---

# Post Method

With Spring, we map requests to request handlers via the @RequestMapping annotation. Spring Boot introduced us to derived types of this annotation - @GetMapping, @PostMapping, @DeleteMapping, etc.

These requests contain different types of information and data - and depending on what our endpoint does with the request, we may want to retrieve the body of a `POST` request either for logging or further processing.

### @RequestBody

The @RequestBody annotation maps body of the web request to the method parameter. The body of the request is passed through an HttpMessageConverter. It resolves the method argument depending on the content type of the request. Optionally, automatic validation can be applied by annotating the argument with @Valid.

In the following example, when we pass the @RequestBody annotation in the createUser() method, it maps to the user parameter.

### @PathMapping

The @PathMapping annotation is the specialized version of the @RequestMapping annotation which acts as a shortcut for @RequestMapping(method=RequestMethod=POST). @PostMapping method handles the Http POST requests matched with the specified URI.

Let's create a user resource and post that resource through the POST method.

**Step 1:** Open UserResource.java and add @PostMapping("/user").

**Step 2:** Create a method createUser() and pass the User class's object as the body of the web.

**Step 3:** Save the created user.

#### UserResource.java

```java
package com.javatpoint.server.main.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResource {
  @Autowired
  private UserDaoService service;

  @GetMapping("/users")
  public List<User> retriveAllUsers() {
    return service.findAll();
  }

  //retrieves a specific user detail  
  @GetMapping("/users/{id}")
  public User retriveUser(@PathVariable int id) {
    return service.findOne(id);
  }

  //method that posts a new user detail   
  @PostMapping("/users")
  public void createUser(@RequestBody User user) {
    User sevedUser = service.save(user);
  }
}  
```
When we refresh the page it returns the get request. But we need to send POST request. We can send POST request through REST client. REST client is a client that is designed to use a service (RESTful) from a server.

**Step 4:** Download [Postman](https://www.getpostman.com/downloads/) 

**Step 5:** Launch the Postman and Signup. 

**Step 6:** First, we check for the GET request. Type the URL http://localhost:8080/users/1 in the address bar and click on the Send button. It returns the detail of the first user.

![Postman](/img/postman1.png "GET request")

**Step 7:** Now we send a POST request.

- Change the method to the POST.
- Copy the body of the response coming from "/users/1".
- Click on Body tab. Now we create a body for the POST request.
- Choose the raw option. It creates a raw request.
- Paste the copied content.
- Remove the id because it increments automatically.
- Change the "name": "Thomas".
- Instead of Text we are sending the data in the JSON format. So select JSON (application/json).
- Type the URL http://localhost:8080/users and click on the Send button.
- Click on the Get request on the left side of the window.
- Now we will send a Get request again, so change the URL http://localhost:8080/users and click on the Send button. It displays all the users, including which we have created.

![Postman](/img/postman2.png "POST request")


## Enhancing POST Method to Return Correct HTTP Code and Status Location

In this section, we will return the status (Created) and URI ("/users/6") of the user recourse which we have created.

### ResponseEntity Class

The ResponseEntity is a class which extends HttpEntity and HttpStatus class. It is defined in org.springframework.http.RequestEntity.

- It is used in RestTemplate and @Controller methods.
- It is used as parameter in getForEntity() and exchange() method.
- It is also used in Spring MVC, as a parameter in a @Controller method.

### created() method of RequestEntity Class

The created() method is the static method of RequestEntity class. It creates a new builder with a CREATED status and a location header set to the given URI.

#### Syntax 

```
public static ResponseEntity.BodyBuilder created(URI location)  
```

**Parameter:** It accepts the location URI as a parameter.

**Returns:** It returns the created builder.

All Http status codes are Enum constant, which is defined in the HttpStatus class.

### ServletUriComponentsBuilder

The ServletUriComponentsBuilder is a class which is defined in org.springframework.web.servlet.support.ServletUriComponentsBuilder. It extends UriComponentsBuilder class. It has additional static factory methods to create a link based on the current HttpServletRequest.

### fromCurrentRequest() method

It is similar to fromRequest(HttpServletRequest) method except the request is obtained through RequestContextHolder.

#### path() method

The path() is the method of UriComponentsBuilder class. It appends the given path to the existing path of this builder. The given path may contain URI template variable.

#### Syntax 

```
public UriBuilderBuilder path(String path)  
```

**Parameter:** It accepts a path as a parameter.

**Returns:** It returns the UriComponentsBuilder.

#### buildAndExpand() method

It builds UriComponents instance and replaces URI template variables with the values obtained from an array. It is the shortcut method which combines calls to build() and then UriComponents.expand(Object... uriVariableValues).

#### Syntax

```
public UriComponents buildAndExpand(Object...uriVariableValues)  
```

**Parameter:** It accepts the URI variable values as a parameter.

**Returns:** It returns the URI components with extended values.

#### build() method

It builds UriComponents instance from the various components contained in the builder.

#### Syntax

```
public UriComponents build()  
```

**Parameter:** It does not accept any parameter.

**Returns:** It returns the Uri Components.

Let's see how to return the status of the created resource and how to set URI of the created resource in the response.

**Step 1:** Create a method that creates a user resource and returns the ResponseEntity.

#### UserResource.java

```java
package com.javatpoint.server.main.user;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserResource {
  @Autowired
  private UserDaoService service;

  @GetMapping("/users")
  public List<User> retriveAllUsers() {
    return service.findAll();
  }

  //retrieves a specific user detail  
  @GetMapping("/users/{id}")
  public User retriveUser(@PathVariable int id) {
    return service.findOne(id);
  }

  //method that posts a new user detail and returns the status of HTTP and location of the user resource  
  @PostMapping("/users")
  public ResponseEntity<Object> createUser(@RequestBody User user) {
    User sevedUser = service.save(user);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(sevedUser.getId()).toUri();
    return ResponseEntity.created(location).build();
  }
}  
```

**Step 2:** Now open REST client Postman and create a POST request.

**Step 3:** Click on the POST request under the History tab.

**Step 4:** Click on the Body tab and change the user name to James.

**Step 5:** Ensure that you have selected JSON (application/json) media type.

**Step 5:** Click on the Send button.

On the right-hand side of the window, we can see the Status: 201 Created. It means resource has been properly created.

**Step 6:** Now click on the Headers tab to see the location. Location is the URI of the created resource. It shows the location of the created user James that is "/users/6".

![Postman](/img/postman3.png "POST request")

If the client wants to know where the user resources was created, just pick up the location from the header of the response.