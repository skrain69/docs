---
sidebar_position: 4
---

# Postman 👨‍🚀

Postman is an application used for API testing. It is an HTTP client that tests HTTP requests, utilizing a graphical user interface, through which we obtain different types of responses that need to be subsequently validated. 

Postman simplifies each step of the API lifecycle and streamlines collaboration so you can create better APIs—faster.
![Postman](/img/postman.svg "What is Postman?")

## Getting Started

<iframe width="950" height="600" src="https://www.youtube.com/embed/fdX0OGvVs5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Introduction to Postman

Below is a 6-part series from Postman where you'll learn the basics of working with a popular API Development Environment (ADE).

### Sending a request
<iframe width="950" height="600" src="https://www.youtube.com/embed/7E60ZttwIpY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Authorizing a request 

<iframe width="950" height="600" src="https://www.youtube.com/embed/Q23wkkfezfM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Writing API tests 

<iframe width="950" height="600" src="https://www.youtube.com/embed/vuVhF257uGw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Advanced API tests 

<iframe width="950" height="600" src="https://www.youtube.com/embed/dDlsQrZmEmo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Chaining requests 

<iframe width="950" height="600" src="https://www.youtube.com/embed/DToBC12Ztnk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Running a collection

<iframe width="950" height="600" src="https://www.youtube.com/embed/la0LqQwwKAA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Postman on Mac OS
🔗 [Intel Chip](https://dl.pstmn.io/download/latest/osx_64) <br />
🔗 [Apple Chip](https://dl.pstmn.io/download/latest/osx_arm64)


## Additional Learning Materials
🔗 [Postman](https://learning.postman.com/docs/getting-started/introduction/)