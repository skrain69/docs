---
sidebar_position: 6
---

# Azure Data Studio ☁️

Azure Data Studio is a cross-platform database tool for data professionals who use on-premises and cloud data platforms on Windows, macOS, and Linux.

Azure Data Studio offers a modern editor experience with IntelliSense, code snippets, source control integration, and an integrated terminal. It's engineered with the data platform user in mind, with the built-in charting of query result sets and customizable dashboards.

Use Azure Data Studio to query, design, and manage your databases and data warehouses wherever they are, on your local computer or in the cloud.

## Getting Started

### Azure Data Studio on Mac OS
🔗 [Azure Data Studio](https://go.microsoft.com/fwlink/?linkid=2176807)

### Download and Installation
<iframe width="950" height="600" src="https://www.youtube.com/embed/s5UEDtsWDHA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## What you can do with Azure Data Studio

### The A to S of Azure Data Studio 
<iframe width="950" height="600" src="https://www.youtube.com/embed/F0bIBFuH93c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Build SQL Database projects easily in Azure Data Studio 
<iframe width="950" height="600" src="https://www.youtube.com/embed/I6T9OA9YBGg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### How to connect to Azure SQL Database from Azure Data Studio
<iframe width="950" height="600" src="https://www.youtube.com/embed/Td_pYlRraQE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [Azure Data Studio](https://docs.microsoft.com/en-us/sql/azure-data-studio/what-is-azure-data-studio?view=sql-server-ver15)

