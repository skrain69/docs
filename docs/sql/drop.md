---
sidebar_position: 0.3
---

# DROP TABLE Statement

The `DROP TABLE` statement is used to drop an existing table in a database.

## Basic syntax

```
DROP TABLE table_name;
```

:::danger caution
Be careful before dropping a table. Deleting a table will result in loss of complete information stored in the table!
:::

## Example

Let us first verify the ***CUSTOMERS*** table and then we will delete it from the sample database as shown below:

```
DESC CUSTOMERS;
```
**Output:**

| Field   | Type          | Null | Key | Default | Extra |
|---------|---------------|------|-----|---------|-------|
| ID      | int(11)       | NO   | PRI |         |       |
| NAME    | varchar(20)   | NO   |     |         |       |
| AGE     | int(11)       | NO   |     |         |       |
| ADDRESS | char(25)      | YES  |     | NULL    |       |
| SALARY  | decimal(18,2) | YES  |     | NULL    |       |

5 rows in set (0.00 sec)

This means that the ***CUSTOMERS*** table is available in the database, so let us now drop it as shown below.

```
DROP TABLE CUSTOMERS;
```
**Output:**

Query OK, 0 rows affected (0.01 sec)

Now, if you would try the DESC command, then you will get the following error:

```
DESC CUSTOMERS;
```

**Output:** 

ERROR 1146 (42S02): Table 'TEST.CUSTOMERS' doesn't exist


