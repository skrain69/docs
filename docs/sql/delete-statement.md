---
sidebar_position: 10
---

# DELETE Statement

The `DELETE` statement is used to delete existing records in a table. 

:::tip 
You can use the `WHERE` clause with the `DELETE` query to delete the selected rows. The `WHERE` clause specifies which record(s) should be deleted. 
:::

## Basic Syntax

```sql
DELETE FROM table_name WHERE condition;
```
## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  Philippines       |

### DELETE Example

The following SQL statement deletes the restaurant "High Street Cafe" from the "Restaurants" table:

```sql
DELETE FROM Restaurants WHERE RestaurantName='High Street Cafe';
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  Philippines       |

### DELETE All Records

It is possible to delete all rows in a table without deleting the table. This means that the table structure, attributes, and indexes will be intact:

#### Syntax 

```sql
DELETE FROM table_name;
```
:::danger Warning
Be careful when deleting records in a table. If you omit the `WHERE` clause, all records in the table will be deleted!
:::

The following SQL statement deletes all rows in the "Restaurants" table, without deleting the table:

```sql
DELETE FROM Restaurants;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|


