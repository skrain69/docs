---
sidebar_position: 16
---

# MIN() and MAX() Functions

The `MIN()` function returns the smallest value of the selected column. <br/>
The `MAX()` function returns the largest value of the selected column.

## Basic Syntax

### MIN() Syntax
```sql
SELECT MIN(column_name)
FROM table_name
WHERE condition;
```

### MAX() Syntax
```sql
SELECT MAX(column_name)
FROM table_name
WHERE condition;
```

## DEMO Database 

Below is an example table named "Groceries":

| ItemID         |     ItemName           |                  Price                            | 
|----------------|:----------------------:|:-------------------------------------------------:|
| 1              |    Liquid Hand Soap    |                115. 1                             |    
| 2              |    Soda                |                 29.5                              |        
| 3              |    Chocolate Bar       |                 67.5                              |            
| 4              |    Ice cream tub       |                313                                |           
| 5              |    Canned fruit        |                 55                                |           
| 6              |    Baby wipes          |                 89                                |           
| 7              |    Toothpaste          |                 231                               |            
| 8              |    Yogurt              |                 110                               |           

### MIN() Example

The following SQL statement finds the price of the cheapest product:

```sql
SELECT MIN(Price) 
FROM Groceries;
```
After executing the above statement, you'll get this output:

| MIN(Price) |
|------------|
| 29.5       |

### MAX() Example

The following SQL statement finds the price of the most expensive product:

```sql
SELECT MAX(Price) 
FROM Groceries;
```
After executing the above statement, you'll get this output:

| MAX(Price) |
|------------|
| 313        |

### Alias

We can also use **Alias** while getting the minimum and maximum value from the table:

```sql
SELECT MAX(Price) AS LargestPrice
FROM Groceries;
```
After executing the above statement, you'll get this output:

| LargestPrice |
|--------------|
| 313          |