---
sidebar_position: 6
---

# Comparison Operators

A comparison (or relational) operator is a mathematical symbol which is used to compare two values.
Comparison operators are used in conditions that compares one expression with another. The result of a comparison can be **TRUE**, **FALSE**, or **UNKNOWN** (an operator that has one or two `NULL` expressions returns **UNKNOWN**).

|Operator	|Description|	Operates on|
|-----------|-----------|--------------|
|=	|Equal to|	Any compatible data types|
|>	|Greater than|	Any compatible data types|
|<	|Less than |Any compatible data types|
|>=	|Greater than equal to|	Any compatible data types|
|<=	|Less than equal to|	Any compatible data types|
|< >	|Not equal to|	Any compatible data types|

## DEMO Database 

Below is an example table named "Employees":

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |       2 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

## Basic Syntax

### (=) Operator

```sql
SELECT * FROM Employees
WHERE salary = 18000;
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |

### (>) Operator

```sql
SELECT * FROM Employees
WHERE salary > 16000;
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |

### (<) Operator

```sql
SELECT * FROM Employees
WHERE salary < 16000;
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |       2 |

### (>=) Operator

```sql
SELECT * FROM Employees
WHERE salary >= 16000;
```
After executing the above statement, you'll get this output:	

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

### (<=) Operator

```sql
SELECT * FROM Employees
WHERE salary <= 16000;
```
After executing the above statement, you'll get this output:	

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |       2 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

### (< >) Operator

```sql
SELECT * FROM Employees
WHERE salary < > 16000;
```
After executing the above statement, you'll get this output:	

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |       2 |

