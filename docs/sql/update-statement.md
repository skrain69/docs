---
sidebar_position: 9
---

# UPDATE Statement

The SQL `UPDATE` Query is used to modify the existing records in a table. 

:::tip 
You can use the `WHERE` clause with the `UPDATE` query to update the selected rows. The `WHERE` clause specifies which record(s) should be updated.
:::

## Basic Syntax

 ```sql
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition;
 ```

## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  Philippines       |


### UPDATE Table

The following SQL statement updates the first restaurant (RestaurantID = 1) with a new address and a new city.

```sql
UPDATE Restaurants
SET Address = 'L/G, Greenbelt 5', City= 'Makati'
WHERE RestaurantID = 1;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      |  L/G, Greenbelt 5                                 |  Makati    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   |  Pasay     |  Philippines       |


### UPDATE Multiple Records

It is the `WHERE` clause that determines how many records will be updated.

The following SQL statement will update the RestaurantName to "Error" for all records where city is "Makati":

```sql
UPDATE Restaurants
SET RestaurantName='Error'
WHERE City='Makati';
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  Error                 |  L/G, Greenbelt 5                                 |  Makati    |   Philippines      |
| 2              |    Error               |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Error                |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   |  Pasay     |  Philippines       |

### UPDATE Warning

:::danger Warning
Be careful when updating records in a table. If you omit the `WHERE` clause, all records in the table will be updated!
:::

```sql
UPDATE Restaurants
SET RestaurantName='Error'
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  Error                 |  L/G, Greenbelt 5                                 |  Makati    |   Philippines      |
| 2              |    Error               |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Error                |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Error                |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Error             |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |   Error                |  2nd Floor, The Newport Mall, Newport Boulevard   |  Pasay     |  Philippines       |