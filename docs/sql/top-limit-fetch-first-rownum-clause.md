---
sidebar_position: 4
---

# TOP, LIMIT, FETCH FIRST or ROWNUM Clause

There are times where you may not be interested in all of the rows returned by a query. For example, you may just want to get the top 3 students by score in your class or the top 10 restaurants by rating in your city. To handle such situations, you can use the `TOP` clause in your `SELECT` statement. <br/>

👉 The `SELECT` `TOP` clause is used to specify the number of records to return.<br/>
👉 The `TOP` clause is useful on large tables with thousands of records. Returning a large number of records can impact performance.

:::info Note
Not all database systems support the `SELECT` `TOP` clause. MySQL provides an equivalent `LIMIT` clause, whereas Oracle provides `ROWNUM` clause for the `SELECT` statement to restrict the number of rows returned by a query.
:::

## Basic Syntax

### SQL Server / MS Access Syntax
```sql
SELECT TOP number|percent column_name(s)
FROM table_name
WHERE condition;
```
### MySQL Syntax
```sql
SELECT column_name(s)
FROM table_name
WHERE condition
LIMIT number;
```
### Oracle 12 Syntax
```sql
SELECT column_name(s)
FROM table_name
ORDER BY column_name(s)
FETCH FIRST number ROWS ONLY;
```
### Older Oracle Syntax
```sql
SELECT column_name(s)
FROM table_name
WHERE ROWNUM <= number;
```

## DEMO Database 

Below is an example table named "Students":

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |
| 010225         | Ben White              |  London                  |  England             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |       
| 010228         |  Jürgen Koch           |  Frankfurt               | Germany              |
| 010229         |  Jürgen Wagner         |  Frankfurt               |  Germany             |
| 010230         | Debbie Fischer         |  Cambridge               | England              |

### SQL TOP, LIMIT, FETCH FIRST and ROWNUM Examples

#### Example I

The following SQL statement selects the first three records from the "Students" table (for SQL Server/MS Access):

```sql
SELECT TOP 3 * FROM Students;
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |

#### Example II

The following SQL statement shows the equivalent example for MySQL:

```sql
SELECT * FROM Students
LIMIT 3;
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |

#### Example III

The following SQL statement shows the equivalent example for Oracle 12:

```sql
SELECT * FROM Students
FETCH FIRST 3 ROWS ONLY;
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |

#### Example IV

The following SQL statement shows the equivalent example for an Older Oracle Server:

```sql
SELECT * FROM Students
WHERE ROWNUM <= 3;
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |


