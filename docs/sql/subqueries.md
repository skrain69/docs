---
sidebar_position: 26
---

# Sub Queries

A Subquery or Inner query or a Nested query is a query within another SQL query and embedded within the `WHERE` clause. It is used to return data that will be used in the main query as a condition to further restrict the data to be retrieved.

:::info Note
Subqueries can be used with the `SELECT`, `INSERT`, `UPDATE`, and `DELETE` statements along with the operators like `=`, `<`, `>`, `>=`, `<=`, `IN`, `BETWEEN`, etc.
:::

There are a few rules that subqueries must follow:

- Subqueries must be enclosed within parentheses.

- A subquery can have only one column in the `SELECT` clause, unless multiple columns are in the main query for the subquery to compare its selected columns.

- An `ORDER BY` command cannot be used in a subquery, although the main query can use an `ORDER BY`. The `GROUP BY` command can be used to perform the same function as the `ORDER BY` in a subquery.

- Subqueries that return more than one row can only be used with multiple value operators such as the `IN` operator.

- The `SELECT` list cannot include any references to values that evaluate to a `BLOB`, `ARRAY`, `CLOB`, or `NCLOB`.

- A subquery cannot be immediately enclosed in a set function.

- The `BETWEEN` operator cannot be used with a subquery. However, the `BETWEEN` operator can be used within the subquery.

## Basic Syntax

```sql
SELECT column_name [, column_name ]
FROM   table1 [, table2 ]
WHERE  column_name OPERATOR
   (SELECT column_name [, column_name ]
   FROM table1 [, table2 ]
   [WHERE])
```

## DEMO Database

Below is an example table named "CUSTOMERS":

| ID | NAME     | AGE | ADDRESS   | SALARY   |
|----|----------|-----|-----------|----------|
|  1 | Ramesh   |  32 | Ahmedabad |  2000.00 |
|  2 | Khilan   |  25 | Delhi     |  1500.00 |
|  3 | kaushik  |  23 | Kota      |  2000.00 |
|  4 | Chaitali |  25 | Mumbai    |  6500.00 |
|  5 | Hardik   |  27 | Bhopal    |  8500.00 |
|  6 | Komal    |  22 | MP        |  4500.00 |
|  7 | Muffy    |  24 | Indore    | 10000.00 |

### Example

The following SQL statement will check among the customers under "CUSTOMERS" table whose SALARY is greater than 4500.

```sql
SQL> SELECT * 
   FROM CUSTOMERS 
   WHERE ID IN (SELECT ID 
         FROM CUSTOMERS 
         WHERE SALARY > 4500) ;
```
After executing the above statement, you'll get this output:


| ID | NAME     | AGE | ADDRESS | SALARY   |
|----|----------|-----|---------|----------|
|  4 | Chaitali |  25 | Mumbai  |  6500.00 |
|  5 | Hardik   |  27 | Bhopal  |  8500.00 |
|  7 | Muffy    |  24 | Indore  | 10000.00 |


