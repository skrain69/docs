---
sidebar_position: 14
---

# Aggregate Functions

An aggregate function allows you to perform a calculation on a set of values to return a single scalar value. We often use aggregate functions with the `GROUP BY` and `HAVING` clauses of the `SELECT` statement.

The following are the most commonly used SQL aggregate functions:

- `AVG` – calculates the average of a set of values.
- `COUNT` – counts rows in a specified table or view.
- `MIN` – gets the minimum value in a set of values.
- `MAX` – gets the maximum value in a set of values.
- `SUM` – calculates the sum of values.

Notice that all aggregate functions above ignore `NULL` values except for the `COUNT` function.

## Basic Syntax

```sql
aggregate_function (DISTINCT | ALL expression)
```

Let’s examine the syntax above in greater detail:

- First, specify an aggregate function that you want to use e.g., `MIN`, `MAX`, `AVG`, `SUM` or `COUNT`.
- Second, put `DISTINCT` or `ALL` modifier followed by an expression inside parentheses. If you explicitly use the `DISTINCT` modifier, the aggregate function ignores duplicate values and only consider the unique values. If you use the `ALL` modifier, the aggregate function uses all values for calculation or evaluation. The `ALL` modifier is used by default if you do not specify any modifier explicitly.