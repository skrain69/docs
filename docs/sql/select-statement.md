---
sidebar_position: 1
---

# SELECT Statement

The `SELECT` statement is used to select data from a database.
You can use this statement to retrieve all the rows from a table in one go, as well as to retrieve only those rows that satisfy a certain condition or a combination of conditions.

## Basic Syntax

The basic syntax for selecting the data from a table can be given with:

```sql
SELECT column1_name, column2_name, column3_name 
FROM table_name;
```
Here, column1_name, column2_name, ... are the names of the columns or fields of a database table whose values you want to fetch.<br/>
However, if you want to fetch the values of all the columns available in a table, you can just use the following syntax:

```sql
SELECT * FROM table_name;
```
## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       | Country            | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |    Makati  | Philippines        |
| 3              | Las Flores             |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |

### SELECT Column Example

If you don't require all the data, you can select specific columns, like **"RestaurantID"** **"RestaurantName"** and **"City"** columns from the "Restaurants" table:

```sql
SELECT RestaurantID, RestaurantName, City 
FROM Restaurants;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |    City       | 
|----------------|:----------------------:|:-------------:|
| 1              |  High Street Cafe      |     Taguig    |  
| 2              |    Blackbird           |     Makati    | 
| 3              | Las Flores             |     Pasig     |   

As you can see there is no **Address** and **Country** columns in the result set.

### SELECT * Example

The following SQL statement selects all the columns from the "Restaurants" table:

```sql
SELECT * FROM Restaurants;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       | Country            | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |    Makati  | Philippines        |
| 3              | Las Flores             |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |

As you can see, it returns all the rows and columns from the 'Restaurants' table.

:::tip 
The asterisk (*) is a wildcard character that means everything. 
For example, the asterisk character in the SELECT statement of the example above 
is a shorthand substitute for all the columns of the employees table.
:::


