---
sidebar_position: 5
---

# Arithmetic Operators

Arithmetic operators are used to performing mathematical calculations such as **Addition**, **Subtraction**, **Multiplication**, **Division**, and **Modulus** on the given operand values. That means these operators are used to perform mathematical operations on two expressions of the same or different data types of numeric data. These different arithmetic operators are as follows:

✔️ Addition ( + ) <br/>
✔️ Subtraction ( - ) <br/>
✔️ Multiplication ( * ) <br/>
✔️ Division ( / ) <br/>
✔️ Modulo ( % )

:::info Note
The + and - operators can also be used in date arithmetic.
:::

|Operator	|Meaning|	Operates on|
|-----------|-------------|------|
|+ (Add)	|Addition	|Numeric value|
|- (Subtract)	|Subtraction	|Numeric value|
|* (Multiply)	|Multiplication	|Numeric value|
|/ (Divide)	|Division	|Numeric value|
|% (Modulo)	|Returns the integer remainder of a division |	Numeric value|

## Basic Syntax

### (+) Operator

```sql
SELECT 40 + 20;
```
After executing the above statement, you'll get this output:

| **40 + 20** | 
|-------------|
|   60        |

### (-) Operator

```sql
SELECT 40 - 20;
```
After executing the above statement, you'll get this output:

| **40 - 20** | 
|-------------|
|   20        |

### (*) Operator

```sql
SELECT 40 * 20;
```
After executing the above statement, you'll get this output:

| **40 * 20** | 
|-------------|
|   800       |

### (/) Operator

```sql
SELECT 40 / 20;
```
After executing the above statement, you'll get this output:

| **40 / 20** | 
|-------------|
|   2         |

### (%) Operator

```sql
SELECT 17 % 5;
```
After executing the above statement, you'll get this output:

| **17 % 5 ** | 
|-------------|
|   2         |