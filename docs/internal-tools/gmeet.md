---
sidebar_position: 5
---

# Google Meet 📹

Google Meet is a video-chatting service designed primarily for business, school and office use, which lets users chat over video and text.

## Getting Started

### How to join a meeting

In Meet, you can select a scheduled event or you can enter a meeting code or nickname.

#### Select a scheduled event

1. In a web browser, enter https://meet.google.com/.
2. Select the meeting from your list of scheduled events. Only meetings scheduled through Google Calendar appear on Google Meet.
3. Click Join now.

:::tip
A chime sounds as the first 5 people join. After that, you receive a silent notification for new participants.
:::

#### Enter a meeting code or nickname

1. In a web browser, enter https://meet.google.com.
2. Click ***Enter a code or link*** > click ***Join***.
3. Enter a meeting code or nickname.

    * The meeting code is the string of letters at the end of the meeting link. You don't have to enter the hyphens.
    * You can only use meeting nicknames with people in your organization. This feature is currently only available to Google Workspace users.
    * If your organization has purchased and installed a Meet hardware device, you can also type the meeting code or nickname into that device.
    * Leave the field blank to start a new meeting with a new code. 

4. Click ***Continue*** and then ***Join now***.

## How Google Meet works
🔗 [Google Meet](https://apps.google.com/intl/en/meet/how-it-works/)



