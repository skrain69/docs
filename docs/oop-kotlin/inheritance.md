---
sidebar_position: 5
---

# Inheritance
It is the concept of creating class hierarchies wherein we override properties and functions of the base class in its subclasses. It is used to access the properties of the base class to a derived class. It is the best approach for **code reusability** in applications from the development point of view. Typically, the **superclass** is known as the base class or the parent class and the **subclasses** are the derived or child class.

```kotlin
class Example // Implicitly inherits from Any
```

All classes in Kotlin have a common superclass `Any`, which has three methods: `equals()`, `hashCode()`, and `toString()`. Thus, these methods are defined for all classes. By default, Kotlin classes are **final** – they can’t be inherited. To make a class inheritable, mark it with the `open` modifier to make it non-final. Also, to allow properties and functions to be overridden we need to set the `open` modifier on them too. To declare an explicit supertype, place the type after a colon in the class header.

```kotlin
open class Base(p: Int)

class Derived(p: Int) : Base(p)
```

If the derived class has a primary constructor, the base class must be initialized in that primary constructor according to its parameters. If it doesn't, each secondary constructor has to initialize the base type using the `super` keyword.

```kotlin
class MyView : View {
  constructor(ctx: Context) : super(ctx)

  constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
}
```

## Single Inheritance
There is a parent class `AccountInfo`, which has two data members and one member method. The subclass `PersonalInfo` is extends its parent class `AccountInfo` using `:` operator. 

```kotlin
open class AccountInfo(var name:String,var number:Int) {  
  fun publicInfo() {  
    println("A/C Name=$name")  
    println("A/C Number=$number")  
  }  
}  

class PersonalInfo:AccountInfo("Mani",101005) {  
  var email:String="trainermani@gmail.com"  
  var contact:Long=8882265032L  
  fun showContactInfo() {  
    println("Email=$email")  
    println("Contact=$contact")  
  }  
}  

fun main(args:Array<String>) {  
  var pi = PersonalInfo() 

  pi.publicInfo()  
  pi.showContactInfo()  
}  

// A/C Name=Mani 
// A/C Number=101005 
// Email=trainermani@gmail.com 
// Contact=8882265032  
```
The object of `PersonalInfo` class is able to access the members of parent class.

## Mulilevel Inheritance
It is when a derived class is again inherited by a child class. There is a parent class `Employee` which has been inherited by its child class `AddressInfo`. This child class is inherited by `ContactsInfo` child class. Inheritance is being performed at more than one level, that's why it's called **multilevel inheritance**.  
```kotlin
open class Employee(var empid:Int) {  
  fun showId() {  
    println("Employee Id = " + empid)  
  }  
}

open class AddressInfo(var address:String,var id:Int):Employee(id) {  
  fun showAddress() {  
    println("Address = $address")  
  }  
}

open class ContactsInfo(var number:Long,var add:String,var userid:Int):AddressInfo(add,userid) {  
  fun showContacts() {  
    println("Contact = $number")  
  }  
} 
 
fun main(args:Array<String>) {  
  var info = ContactsInfo(8882265032,"Delhi",5001)  
  info.showId()  
  info.showAddress()  
  info.showContacts()  
}  

// Employee Id = 5001 
// Address = Delhi 
// Contact = 8882265032 
```
The object of `ContactsInfo` class is able to access the `showId()` function from `Employee` parent class, `showAddress()` from `AddressInfo`, and `showContacts()` from `ContactsInfo` class.

## Additional Learning Materials
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/inheritance.html)<br/>
🔗 [Journal Dev](https://www.journaldev.com/19705/kotlin-inheritance)
